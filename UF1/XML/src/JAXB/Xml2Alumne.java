package JAXB;

import java.io.File;  
import java.util.List;  
  
import javax.xml.bind.JAXBContext;  
import javax.xml.bind.JAXBException;  
import javax.xml.bind.Unmarshaller;  
   
public class Xml2Alumne {  
    public static void main(String[] args) {  
   
     try {  
   
    	///<magia negra>
        File file = new File("alumne");  
        JAXBContext jaxbContext = JAXBContext.newInstance(Alumne.class);  
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
        Alumne alum= (Alumne) jaxbUnmarshaller.unmarshal(file);  
        ///</magia negra>
        
        
        System.out.println(alum.getId()+" "+alum.getNom()+" "+alum.getAltura());  
          
        
      } catch (JAXBException e) {  
        e.printStackTrace();  
      }  
   
    }  
}  
