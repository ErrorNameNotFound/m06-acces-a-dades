package com.marc.jsonsimple;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.omg.Messaging.SyncScopeHelper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class llegirJSON {

    public static void main(String[] args) {

    	//magia negra
        JSONParser parser = new JSONParser();

        try {
        	//magia negra
            Object obj = parser.parse(new FileReader("testjson"));

            JSONObject jsonObject = (JSONObject) obj;
            System.out.println(jsonObject);

            System.out.println(jsonObject.keySet());
            String name = (String) jsonObject.get("nom");
            System.out.println(name);

            long age = (Long) jsonObject.get("edad");
            System.out.println(age);

            // loop array
            JSONArray aficions = (JSONArray) jsonObject.get("aficions");

            for(Object s : aficions) {
            	System.out.println(s);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}